package com.example.droolsdemo.yunding;

import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONUtil;
import com.example.droolsdemo.yunding.model.PageResult;
import com.example.droolsdemo.yunding.model.Result;
import com.example.droolsdemo.yunding.model.compose.AutoComposeRequest;
import com.example.droolsdemo.yunding.model.compose.ComposeListQuery;
import com.example.droolsdemo.yunding.model.compose.CreateComposingRequest;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import okhttp3.*;
import okio.BufferedSink;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @author shenxuehai
 * @description 合成
 * @Date 2023-03-28-22:26
 **/
public class ComposeRunner {

    public static final String listApi = "/api/mix/lists";

    public static final String AUTO_COMPOSING_API = "/api/mix/auto";
    public static final String CREATE_COMPOSING_API = "/api/mix/create";


    public final OkHttpClient client = new OkHttpClient.Builder()
            .followRedirects(true)
            .followSslRedirects(true)
            .connectTimeout(3, TimeUnit.SECONDS)
            .build();



    public void startComposing(String id) {
        String poll = YundingOrderRunner.queue.poll();
        YundingOrderRunner.queue.add(poll);
        AutoComposeRequest composeRequest = new AutoComposeRequest();
        composeRequest.setId(id);
        String json = JSONUtil.toJsonStr(composeRequest);
        Request request = new Request.Builder()
                .addHeader("token", YundingOrderRunner.TOKEN)
                .url("https://" + poll + AUTO_COMPOSING_API)
                .post(new RequestBody() {
                    @Override
                    public MediaType contentType() {
                        return MediaType.get("application/json");
                    }

                    @Override
                    public void writeTo(@NotNull BufferedSink bufferedSink) throws IOException {
                        bufferedSink.write(json.getBytes());
                    }
                })
                .build();
        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {

            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                String responseText = Objects.requireNonNull(response.body()).string();
                Map map = JSONUtil.toBean(responseText, Map.class);
                Object code = map.get("code");
                if (Integer.parseInt(code + "") == 1) {
                    List<Map<String, Object>> list = (List<Map<String, Object>>) map.get("data");
                    createComposing(id, list);
                }

            }
        });

    }

    public void createComposing(String id, List<Map<String, Object>> list) {
        String poll = YundingOrderRunner.queue.poll();
        YundingOrderRunner.queue.add(poll);
        List<Set<String>> dataList = new ArrayList<>();
        List<String> products = new ArrayList<>();
        list.forEach(item -> {
            dataList.add(item.keySet());
            for (String s : item.keySet()) {
                Map<String, Object> o = (Map<String, Object>) item.get(s);
                Object name = o.get("name");
                products.add(name + "");
            }
        });
        System.out.println("填入成功:" + products);
        CreateComposingRequest createComposingRequest = new CreateComposingRequest();
        createComposingRequest.setSelect(dataList);
        createComposingRequest.setId(Integer.parseInt(id));
        String s = JSONUtil.toJsonStr(createComposingRequest);
        System.out.println(s);
        Request request = new Request.Builder()
                .addHeader("token", YundingOrderRunner.TOKEN)
                .url("https://" + "api.vvtok.com" + CREATE_COMPOSING_API)
                .post(new FormBody.Builder().add("id", id).add("select", JSONUtil.toJsonStr(dataList)).build())
                .build();
        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    throw new RuntimeException(ex);
                }
                createComposing(id, list);
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                String string = Objects.requireNonNull(response.body()).string();
                try {
                    Result result = JSONUtil.toBean(string, Result.class);
                    if (result.getCode() == 1) {
                        System.out.println("合成成功");
                    }else {
                        System.out.println(result);
                        Thread.sleep(1000);
//                        createComposing(id, list);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
                        throw new RuntimeException(ex);
                    }
                    createComposing(id, list);
                }


            }
        });
    }


    public void getComposeList() {
        String poll = YundingOrderRunner.queue.poll();
        YundingOrderRunner.queue.add(poll);
        ComposeListQuery query = new ComposeListQuery();
        Gson gson = new Gson();
        String json = gson.toJson(query);
        Request request = new Request.Builder()
                .addHeader("token", YundingOrderRunner.TOKEN)
                .url("https://" + poll + listApi)
                .post(new RequestBody() {
                    @Override
                    public MediaType contentType() {
                        return MediaType.get("application/json");
                    }

                    @Override
                    public void writeTo(@NotNull BufferedSink bufferedSink) throws IOException {
                        bufferedSink.write(json.getBytes());
                    }
                })
                .build();
        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {

            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                String string = Objects.requireNonNull(response.body()).string();
                System.out.println(string);
            }
        });

    }

}
