package com.example.droolsdemo.yunding;

import cn.hutool.extra.mail.MailUtil;
import com.example.droolsdemo.yunding.model.Result;
import com.example.droolsdemo.yunding.model.product.*;
import com.google.gson.*;
import okhttp3.*;
import okio.BufferedSink;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.*;
import javax.xml.crypto.Data;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;

/**
 * @author shenxuehai
 * @description
 * @Date 2022-08-15-15:38
 **/
public class YundingOrderRunner {
    
    public static Logger logger = LoggerFactory.getLogger(YundingOrderRunner.class);

    // 用户token
    public static final String TOKEN = "9eee6b98-7b51-48cb-ae81-47b61385f3ae";
//    public static final String TOKEN = "9d11058e-cba0-4ab8-a2a7-b59a54540692";

    // 支付密码
    public static final String pwd = "842533";

    // 用户邮箱
    private static final String mail = "8425334@qq.com";

    // 最高价自动买入监控
    public static final double MAX_BUY = 1;

    // 最低价自动买入监控
    public static final double MIN_BUY = 0;

    public static final boolean AUTO_SELL = false;

    public static final boolean AUTO_BUY = false;

    public static Queue<String> queue = new LinkedList<>();




    public static void main(String[] args) throws InterruptedException, IOException {
        logger.info("欢迎使用云顶抢购小助手 v1.0");
//         普通抢购
        genPriceMap();
        queue.add("39.108.164.156");
        queue.add("39.108.68.0");
        queue.add("39.108.1.12");
        queue.add("39.108.76.192");
        queue.add("39.108.170.63");
        queue.add("39.108.175.194");
        queue.add("120.77.204.114");
//        queue.add("api.vvtok.com");
//        queue.add("h5.vvtok.com");
//        queue.add("pc.vvtok.com");
//        ComposeRunner runner = new ComposeRunner();
//        for (int i =0; i <= 100; i++) {
//            runner.startComposing("47");
//            Thread.sleep(1000);
//        }

        while (true) {
            try {
//                logger.info("=== 开始监控，第" + i++ + "次获取 ===");
                getGoodList();
                Thread.sleep(200);
//                logger.info("====== 结束监控 ====");
            } catch (Exception e) {
                logger.warn("【报错】:{}", e.getMessage());
            } finally {
                System.gc();
            }
        }

//        // 首发抢购
//        Product product = new Product();
//        product.setId(72);
//        createOrder(product);
    }

    static Map<String, Price> priceMap = new HashMap<>();

    // 藏品哈希表，低于价格买入=
    private static void genPriceMap() {
        priceMap.put("侦查·安娜", new Price(20.00));
        priceMap.put("蓝色妖姬", new Price(50.00));
        priceMap.put("警长·娜塔莎", new Price(15.0));
        priceMap.put("宇宙原石", new Price(21.0));
        priceMap.put("维修·卡莎尔", new Price(250.0));
        priceMap.put("魔兽三国·炽焰关羽", new Price(90.00));
        priceMap.put("创世女神·娲皇补天 ", new Price(900.00));
        priceMap.put("西路财神关羽", new Price(10.00));
        priceMap.put("东路财神比干", new Price(10.00));
        priceMap.put("浪客行", new Price(20.00));
        priceMap.put("机甲马超", new Price(200.00));
        priceMap.put("魔兽三国·英雄刘备", new Price(100.00));
        priceMap.put("南路财神柴王爷", new Price(7.00));

        priceMap.put("五灵瑞象", new Price(140.00));
        priceMap.put("机甲张飞", new Price(300.00));
        priceMap.put("机甲黄忠", new Price(300.00));
        priceMap.put("机甲赵云", new Price(300.00));
        priceMap.put("刘备·双股剑", new Price(70.00));
        priceMap.put("关羽·青龙偃月刀", new Price(45.00));
        priceMap.put("机甲诸葛亮", new Price(200.00));
        priceMap.put("山海经", new Price(200.00));
        priceMap.put("星空幻想", new Price(20.00));
        priceMap.put("云顶龙", new Price(300.00));
        priceMap.put("机甲关羽", new Price(100.00));
        priceMap.put("山雨欲来", new Price(28.00));
        priceMap.put("炮手·玛莎", new Price(200.00));
        priceMap.put("弓箭手·克莱尔", new Price(51.00));
        priceMap.put("北路财神赵公明", new Price(11.00));
        priceMap.put("九天龙域", new Price(11.00));
        priceMap.put("楚国骑兵", new Price(110.00));
        priceMap.put("楚国弓弩兵", new Price(120.00));
        priceMap.put("江东精骑", new Price(501.00));
        priceMap.put("虞姬", new Price(500.00));
        priceMap.put("创世·斗战胜佛", new Price(1401.00));
        priceMap.put("汉盾兵", new Price(19.0));
        priceMap.put("楚盾兵", new Price(41.0));
        priceMap.put("云顶徽章", new Price(51.0));
        priceMap.put("韩信", new Price(1400.0));
        priceMap.put("项羽", new Price(500.0));
        priceMap.put("驯服", new Price(121.0));

    }

    // 商品列表获取
    private static OkHttpClient goodsClient =
            new OkHttpClient.Builder()
                    .followSslRedirects(true)
                    .followRedirects(true)
                    .build();
    static int i = 1;

    private static Map<Long, Void> productMap = new HashMap<>();
    private static void getGoodList() throws IOException {
        String poll = queue.poll();
        queue.add(poll);
//        logger.info("【api】{}", poll);
//        logger.info("【host】:{}", poll);
        Connection.Response execute = Jsoup.connect("https://" + poll + "/api/basic/product")
                .header("token", TOKEN)
                .timeout(3000)
                .followRedirects(true)
                .header("Access-Control-Allow-Credentials", "true")
                .header("Access-Control-Allow-Origin", "https://api.vvtok.com")
                .referrer("https://api.vvtok.com")
                .ignoreContentType(true)
                .ignoreHttpErrors(true)
                .userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36")
                .execute();
        Document doc = execute.parse();
//        Document doc = Jsoup.connect("https://39.108.68.0/api/basic/product")
//                .get();
        String json = doc.body().html();
//        logger.info(json);
        if (!json.contains("<html>")) {
            Gson gson = new Gson();
//                            logger.debug("获取商品列表成功：{}", json);
            JsonObject jsonObject = new JsonParser().parse(json)
                    .getAsJsonObject();
            JsonArray jsonArray = null;
            try {
//                logger.info("goods:{}", json);
                if (jsonObject.get("code").getAsInt() != 1) {
                    logger.error("获取商品失败:{}", json);
                }
                jsonArray = jsonObject.get("data").getAsJsonObject().get("data").getAsJsonArray();
            } catch (Exception e) {
                logger.info("获取列表出错，商品列表数据：{}", json);
                e.printStackTrace();
            }
//            logger.info("======【获取各个商品价格】======");
//            StringBuilder stringBuilder = new StringBuilder();
            for (JsonElement element : jsonArray) {
                Product product = gson.fromJson(element, Product.class);
                if (!productMap.containsKey(product.getId())) {
                    productMap.put(product.getId(), null);
                    if (productMap.size() > 15) {
                        logger.info("【发现新商品】名称:{}, 价格:{}", product.getName(), product.getSellPrice());
                    }
                }
//                stringBuilder.append("【")
//                                .append(product.getName()).append("】").append(product.getSellPrice())
//                                .append("；");
                Double price = Double.parseDouble(product.getSellPrice());
                if (product.getStatus().equals("5")) {
                    if (priceMap.containsKey(product.getName())) {
                        Price p = priceMap.get(product.getName());
                        Double buyPrice = p.getBuyPrice();
                        if (price < buyPrice) {
                            logger.info("触发map价，name: {},originPrice:{}, buy price:{}, sell price:{}",
                                    product.getName(),
                                    product.getSellPrice(),
                                    buyPrice,
                                    p.getSellPrice());
                            createOrder(product);
                        }
                    } else {
                        logger.info("未匹配到商品存在,name:{} ", product.getName());
                        if (price < MIN_BUY) {
                            logger.info("触发最低价买入：product:{}", product);
                            createOrder(product);
                        }
                    }
                }
            }
//            logger.info(stringBuilder.toString());
//            logger.info("======【结束获取各个商品价格】======");
        } else {
            logger.info("请求错误:{}",  json);
        }
//        String s = doc.body().toString();
//        Request request = new Request.Builder()
//                .url("https://"+ poll  +"/api/basic/product")
//                .addHeader("token", TOKEN)
//                .post(new RequestBody() {
//                    @Nullable
//                    @Override
//                    public MediaType contentType() {
//                        return MediaType.parse("application/json");
//                    }
//
//                    @Override
//                    public void writeTo(@NotNull BufferedSink bufferedSink) throws IOException {
//                        Gson gson = new Gson();
//                        String s = gson.toJson(new ProductRequest());
//                        bufferedSink.write(s.getBytes());
//                    }
//                })
//                .build();
////        logger.info("====开始获取商品====");
//        goodsClient.newCall(request)
//                .enqueue(new Callback() {
//                    @Override
//                    public void onFailure(@NotNull Call call, @NotNull IOException e) {
//                        e.printStackTrace();
//                    }
//
//                    @Override
//                    public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
//                        String json = response.body().string();
//                        if (!json.contains("<html>")) {
//                            Gson gson = new Gson();
////                            logger.debug("获取商品列表成功：{}", json);
//                            JsonObject jsonObject = new JsonParser().parse(json)
//                                    .getAsJsonObject();
//                            JsonArray jsonArray = null;
//                            try {
//                                logger.info("goods:{}", json);
//                                if (jsonObject.get("code").getAsInt() != 1) {
//                                    logger.error("获取商品失败:{}", json);
//                                }
//                                jsonArray = jsonObject.get("data").getAsJsonObject().get("data").getAsJsonArray();
//                            } catch (Exception e) {
//                                logger.info("获取列表出错，商品列表数据：{}", json);
//                                e.printStackTrace();
//                            }
//
//                            for (JsonElement element : jsonArray) {
//                                Product product = gson.fromJson(element, Product.class);
//                                Double price = Double.parseDouble(product.getSellPrice());
//                                if (product.getStatus().equals("5")) {
//                                    if (priceMap.containsKey(product.getName())) {
//                                        Price p = priceMap.get(product.getName());
//                                        Double buyPrice = p.getBuyPrice();
//                                        if (price < buyPrice) {
//                                            logger.info("触发map价，name: {},originPrice:{}, buy price:{}, sell price:{}",
//                                                    product.getName(),
//                                                    product.getSellPrice(),
//                                                    buyPrice,
//                                                    p.getSellPrice());
//                                            createOrder(product);
//                                        }
//                                    } else {
//                                        logger.info("未匹配到商品存在,name:{} ", product.getName());
//                                        if (price < MIN_BUY) {
//                                            logger.info("触发最低价买入：product:{}", product);
//                                            createOrder(product);
//                                        }
//                                    }
//                                }
//                            }
//                        } else {
//                            logger.info("请求错误:{}",  json);
//                        }
////                        logger.info("====结束请求商品列表====");
//                    }
//                });

    }


    /**
     * 创建订单
     */
    private static OkHttpClient createOrderClient = new OkHttpClient();
    private static void createOrder(Product product) throws IOException {
//        String poll = queue.poll();
//        queue.add(poll);
        Request request = new Request.Builder()
                .url("https://" + "api.vvtok.com" + "/api/works/creatOrders")
                .header("token", TOKEN)
                .post(new RequestBody() {
                    @Nullable
                    @Override
                    public MediaType contentType() {
                        return MediaType.parse("application/json");
                    }

                    @Override
                    public void writeTo(@NotNull BufferedSink bufferedSink) throws IOException {
                        OrderRequest orderRequest = new OrderRequest();
                        orderRequest.setId(product.getId() + "");
                        Gson gson = new Gson();
                        String s = gson.toJson(orderRequest);
                        logger.info("create order:{}, product:{}", s, product);
                        bufferedSink.write(s.getBytes());
                    }
                })
                .build();
        Response execute = createOrderClient.newCall(request).execute();
        String json = execute.body().string();
        Gson gson = new Gson();
        Result<OrderResult> result = gson.fromJson(json, Result.class);
        logger.info("抢购结果:{}", json);
        if (result.getCode() == 1) {
            MailUtil.send(
                    mail,
                    "云顶自动抢购提醒" + nowFormatDate(),
                    "抢购商品：" + product.getName() +
                            "<p>商品价格：" + product.getSellPrice() +
                            "</p><p>抢购时间:" + nowFormatDate() +
                            "</p><p>抢购结果："+ json +
                            "</p>",
            true);
            JsonObject jsonObject = new JsonParser().parse(json)
                    .getAsJsonObject();
            JsonElement element = jsonObject.get("data");
            OrderResult orderResult = gson.fromJson(element, OrderResult.class);
//            if (MAX_BUY < Double.parseDouble(product.getSellPrice())) {
//                MailUtil.send(
//                        mail,
//                        "云顶手动支付提醒",
//                        "抢购商品：" + product +
//                                "\n抢购时间:" + new Date().toString(),
//                        false);
//            }
            if (AUTO_BUY && MAX_BUY > Double.parseDouble(product.getSellPrice())) {
                buyOrder(product, orderResult);
            }
        }
        logger.info(json);
    }


    // 买入order
    private static OkHttpClient buyClient = new OkHttpClient();
    private static void buyOrder(Product product, OrderResult result) {
        // 休眠2秒再支付
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        BuyRequest buyRequest = new BuyRequest();
        buyRequest.setOrder_id(result.getId());
        buyRequest.setBank_id("");
        buyRequest.setClient("app");
        buyRequest.setPay_pwd(pwd);
        buyRequest.setPay_status("money");
        Gson gson = new Gson();
        String s = gson.toJson(buyRequest);
        Request request = new Request.Builder()
                .url("https://api.vvtok.com/api/works/payOrder")
                .header("token", TOKEN)
                        .post(new RequestBody() {
                            @Nullable
                            @Override
                            public MediaType contentType() {
                                return MediaType.parse("application/json");
                            }

                            @Override
                            public void writeTo(@NotNull BufferedSink bufferedSink) throws IOException {
                                logger.info("开始自动支付：{}", s);
                                bufferedSink.write(s.getBytes());
                            }
                        })
                                .build();
        buyClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {

            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                String json = response.body().string();
                Gson g = new Gson();
                Result r = g.fromJson(json, Result.class);
                logger.info("支付结果：{}", json);
                if (r.getCode() == 1) {
                    MailUtil.send(
                            mail,
                            "云顶自动支付成功提醒" + nowFormatDate(),
                            "<p>抢购商品：" + product.getName() +
                                    "</p><p>商品价格：" + product.getSellPrice() +
                                    "</p><p>抢购时间:" + nowFormatDate() +
                                    "</p><p>支付请求：" + s +
                                    "</p><p>支付结果："+ json +
                                    "</p>",
                            false);
                    try {
                        if (AUTO_SELL) {
                            sellGoods(product);
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } else {
                    MailUtil.send(
                            mail,
                            "云顶自动支付失败提醒",
                            "<p>抢购商品：" + product.getName() +
                                    "</p><p>商品价格：" + product.getSellPrice() +
                                    "</p><p>抢购时间:" + nowFormatDate() +
                                    "</p><p>支付请求：" + s +
                                    "</p><p>支付结果："+ json +
                                    "</p>",
                            true);
                }
            }
        });
    }

    // 自动卖出逻辑
    private static OkHttpClient sellClient = new OkHttpClient();
    public static void sellGoods(Product product) throws IOException, InterruptedException {
        new Thread(new Runnable() {
            @Override
            public void run() {
                // 休眠10秒再卖出
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (priceMap.containsKey(product.getName())) {
                    Price price = priceMap.get(product.getName());
                    // 高于66元暂不卖出，看情况
                    if (price.getBuyPrice() > 66.00) {
                        return;
                    }
                    double sellPrice = price.getSellPrice();
                    // 小于30元的商品再加价5%
//                    if (price.getBuyPrice() < 50) {
//                        sellPrice = price.getSellPrice() + price.getSellPrice() * 0.05;
//                    }
                    SellRequest sellRequest = new SellRequest();
                    sellRequest.setProduct_id(product.getId() + "");
                    sellRequest.setPay_pwd(pwd);
                    sellRequest.setSellprice(sellPrice + "");
                    Gson gson = new Gson();
                    String s = gson.toJson(sellRequest);
                    logger.info("开始自动卖出：{}",  s);
                    Request request = new Request.Builder()
                            .url("https://api.vvtok.com/api/works/setSell")
                            .header("token", TOKEN)
                            .post(new RequestBody() {
                                @Nullable
                                @Override
                                public MediaType contentType() {
                                    return MediaType.parse("application/json");
                                }

                                @Override
                                public void writeTo(@NotNull BufferedSink bufferedSink) throws IOException {
                                    logger.info("开始自动卖出：{}",  s);
                                    bufferedSink.write(s.getBytes());
                                }
                            })
                            .build();
                    Response execute = null;
                    try {
                        execute = sellClient.newCall(request).execute();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    String resultStrig = null;
                    try {
                        resultStrig = execute.body().string();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    logger.info("卖出结果：{}",  resultStrig);
                    Gson g = new Gson();
                    Result result = g.fromJson(resultStrig, Result.class);
                    if (result.getCode() == 1) {
                        MailUtil.send(
                                mail,
                                "云顶自动卖出提醒" + nowFormatDate(),
                                "<p>抢购商品：" + product.getName() +
                                        "</p><p>买入价格：" + product.getSellPrice() +
                                        "</p><p>卖出价格: " + price.getSellPrice() +
                                        "</p><p>卖出时间:" + nowFormatDate() +
                                        "</p>",
                                true);
                    } else {
                        MailUtil.send(
                                mail,
                                "云顶自动卖出失败" + nowFormatDate(),
                                "<p>抢购商品：" + product.getName() +
                                        "</p><p>买入价格：" + product.getSellPrice() +
                                        "</p><p>卖出价格: " + price.getSellPrice() +
                                        "</p><p>卖出时间:" + nowFormatDate() +
                                        "</p><p>请求参数：" + s +
                                        "</p><p>返回参数：" + resultStrig +
                                        "</p>",
                                true);
                    }
                }
            }
        }).start();
    }


    private static String nowFormatDate() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        return format.format(new Date());
    }

    static {
        disableSslVerification();
    }

    private static void disableSslVerification() {
        try
        {
            // Create a trust manager that does not validate certificate chains
            TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }
            };

            // Install the all-trusting trust manager
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };

            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
    }


}
