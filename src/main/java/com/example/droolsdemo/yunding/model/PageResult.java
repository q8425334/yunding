package com.example.droolsdemo.yunding.model;

import lombok.Data;
import lombok.ToString;
import java.util.List;

/**
 * @author shenxuehai
 * @description
 * @Date 2023-03-28-22:40
 **/

@Data
@ToString
public class PageResult <P> {

    private int total;

    private int per_page;

    private int current_page;

    private List<P> data;

}
