package com.example.droolsdemo.yunding.model.compose;

import lombok.Data;

/**
 * @author shenxuehai
 * @description
 * @Date 2023-03-28-22:52
 **/
@Data
public class AutoComposeRequest {

    private String id;
}
