package com.example.droolsdemo.yunding.model.compose;

import lombok.Data;

/**
 * @author shenxuehai
 * @description
 * @Date 2023-03-28-22:30
 **/
@Data
public class ComposeListQuery {

    private int page = 1;

    private int limit = 15;


}
