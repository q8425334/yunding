package com.example.droolsdemo.yunding.model.compose;

import lombok.Data;

import java.util.List;
import java.util.Set;

/**
 * @author shenxuehai
 * @description
 * @Date 2023-03-28-23:23
 **/
@Data
public class CreateComposingRequest {

    private Integer id;

    private List<Set<String>> select;
}
