package com.example.droolsdemo.yunding.model.dinglian;

import java.io.Serializable;
import java.lang.Boolean;
import java.lang.Double;
import java.lang.Integer;
import java.lang.Long;
import java.lang.String;

public class DinglianProduct implements Serializable {
  private Integer highestPrice;

  private Integer lowestPrice;

  private Integer dislikeCount;

  private String albumId;

  private Integer currentPrice;

  private Integer likeCount;

  private Boolean collected;

  private String platformId;

  private String type;

  private Double increase;

  private Boolean isSubscribe;

  private Platform platform;

  private Long createdAt;

  private String intervalPrice;

  private Double deal24hSum;

  private Boolean blocked;

  private String imageUrl;

  private String name;

  private Integer deal24hCount;

  private String id;

  public Integer getHighestPrice() {
    return this.highestPrice;
  }

  public void setHighestPrice(Integer highestPrice) {
    this.highestPrice = highestPrice;
  }

  public Integer getLowestPrice() {
    return this.lowestPrice;
  }

  public void setLowestPrice(Integer lowestPrice) {
    this.lowestPrice = lowestPrice;
  }

  public Integer getDislikeCount() {
    return this.dislikeCount;
  }

  public void setDislikeCount(Integer dislikeCount) {
    this.dislikeCount = dislikeCount;
  }

  public String getAlbumId() {
    return this.albumId;
  }

  public void setAlbumId(String albumId) {
    this.albumId = albumId;
  }

  public Integer getCurrentPrice() {
    return this.currentPrice;
  }

  public void setCurrentPrice(Integer currentPrice) {
    this.currentPrice = currentPrice;
  }

  public Integer getLikeCount() {
    return this.likeCount;
  }

  public void setLikeCount(Integer likeCount) {
    this.likeCount = likeCount;
  }

  public Boolean getCollected() {
    return this.collected;
  }

  public void setCollected(Boolean collected) {
    this.collected = collected;
  }

  public String getPlatformId() {
    return this.platformId;
  }

  public void setPlatformId(String platformId) {
    this.platformId = platformId;
  }

  public String getType() {
    return this.type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Double getIncrease() {
    return this.increase;
  }

  public void setIncrease(Double increase) {
    this.increase = increase;
  }

  public Boolean getIsSubscribe() {
    return this.isSubscribe;
  }

  public void setIsSubscribe(Boolean isSubscribe) {
    this.isSubscribe = isSubscribe;
  }

  public Platform getPlatform() {
    return this.platform;
  }

  public void setPlatform(Platform platform) {
    this.platform = platform;
  }

  public Long getCreatedAt() {
    return this.createdAt;
  }

  public void setCreatedAt(Long createdAt) {
    this.createdAt = createdAt;
  }

  public String getIntervalPrice() {
    return this.intervalPrice;
  }

  public void setIntervalPrice(String intervalPrice) {
    this.intervalPrice = intervalPrice;
  }

  public Double getDeal24hSum() {
    return this.deal24hSum;
  }

  public void setDeal24hSum(Double deal24hSum) {
    this.deal24hSum = deal24hSum;
  }

  public Boolean getBlocked() {
    return this.blocked;
  }

  public void setBlocked(Boolean blocked) {
    this.blocked = blocked;
  }

  public String getImageUrl() {
    return this.imageUrl;
  }

  public void setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getDeal24hCount() {
    return this.deal24hCount;
  }

  public void setDeal24hCount(Integer deal24hCount) {
    this.deal24hCount = deal24hCount;
  }

  public String getId() {
    return this.id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public static class Platform implements Serializable {
    private Long createdAt;

    private Boolean blocked;

    private String imageUrl;

    private String name;

    private String id;

    private Boolean silence;

    private String type;

    private String forwardUrl;

    private Boolean followed;

    public Long getCreatedAt() {
      return this.createdAt;
    }

    public void setCreatedAt(Long createdAt) {
      this.createdAt = createdAt;
    }

    public Boolean getBlocked() {
      return this.blocked;
    }

    public void setBlocked(Boolean blocked) {
      this.blocked = blocked;
    }

    public String getImageUrl() {
      return this.imageUrl;
    }

    public void setImageUrl(String imageUrl) {
      this.imageUrl = imageUrl;
    }

    public String getName() {
      return this.name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public String getId() {
      return this.id;
    }

    public void setId(String id) {
      this.id = id;
    }

    public Boolean getSilence() {
      return this.silence;
    }

    public void setSilence(Boolean silence) {
      this.silence = silence;
    }

    public String getType() {
      return this.type;
    }

    public void setType(String type) {
      this.type = type;
    }

    public String getForwardUrl() {
      return this.forwardUrl;
    }

    public void setForwardUrl(String forwardUrl) {
      this.forwardUrl = forwardUrl;
    }

    public Boolean getFollowed() {
      return this.followed;
    }

    public void setFollowed(Boolean followed) {
      this.followed = followed;
    }
  }
}
