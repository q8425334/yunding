package com.example.droolsdemo.yunding.model.dinglian;

/**
 * @author shenxuehai
 * @description
 * @Date 2022-08-19-19:53
 **/
public class DinglianResult<T> {

    private Boolean err;

    private T res;

    public Boolean getErr() {
        return err;
    }

    public void setErr(Boolean err) {
        this.err = err;
    }

    public T getRes() {
        return res;
    }

    public void setRes(T res) {
        this.res = res;
    }
}
