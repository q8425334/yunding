package com.example.droolsdemo.yunding.model.product;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @author Deb
 * @date 2022/8/16
 **/
public class Price {

    // 买入价格
    private Double buyPrice;

    // 卖出价格
    private Double sellPrice;

    private Boolean autoBuy;

    public Price(Double buyPrice, Boolean autoBuy) {
        this.buyPrice = buyPrice;
        this.sellPrice = this.buyPrice + this.buyPrice * 0.10;
        this.autoBuy = autoBuy;
    }

    public Price(Double buyPrice) {
        this.buyPrice = buyPrice;
        this.sellPrice = this.buyPrice + this.buyPrice * 0.10;
        this.autoBuy = false;
    }

    /**
     * 按照比率卖出
     * @param buyPrice 买入价格
     * @param sellPercent 卖出比率
     */
    public Price(Double buyPrice, Float sellPercent) {
        this.buyPrice = buyPrice;
        this.sellPrice = this.buyPrice + this.buyPrice * sellPercent;
    }

    public Price(Double buyPrice, Double sellPrice) {
        this.buyPrice = buyPrice;
        this.sellPrice = sellPrice;
    }

    public Double getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(Double buyPrice) {
        this.buyPrice = buyPrice;
    }

    public Double getSellPrice() {
        return new BigDecimal(sellPrice).setScale(2, RoundingMode.HALF_UP).doubleValue();
    }

    public void setSellPrice(Double sellPrice) {
        this.sellPrice = sellPrice;
    }

    public Boolean getAutoBuy() {
        return autoBuy;
    }

    public void setAutoBuy(Boolean autoBuy) {
        this.autoBuy = autoBuy;
    }
}
