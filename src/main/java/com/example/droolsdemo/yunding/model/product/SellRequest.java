package com.example.droolsdemo.yunding.model.product;

/**
 * @author Deb
 * @date 2022/8/17
 **/
public class SellRequest {

    private String product_id;

    private String sellprice;

    private String pay_pwd;

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getSellprice() {
        return sellprice;
    }

    public void setSellprice(String sellprice) {
        this.sellprice = sellprice;
    }

    public String getPay_pwd() {
        return pay_pwd;
    }

    public void setPay_pwd(String pay_pwd) {
        this.pay_pwd = pay_pwd;
    }
}
